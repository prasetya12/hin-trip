export const API = {
    LOGIN : 'auth/login',
    ATTRACTIONS:'attraction/list',
    DETAIL_ATTRACTION:'attraction/get',
    TICKET_TYPE:'ticketType/list',
    CREATE_BOOKING:'transaction/create'
}
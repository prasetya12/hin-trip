import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {of as observableOf,  Observable } from 'rxjs';
import {delay,tap} from 'rxjs/operators';
import {API} from '../../constants/urls'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}
  public authTokenNew: string = 'new_auth_token';

  refreshToken():Observable<string>{
    const data = {
                "username": "reseller@globaltix.com",
                "password": "12345"
                }

    return this.http.post<any>(API.LOGIN, data)
      .pipe(tap((result: any) => {
        localStorage.setItem('token', JSON.stringify(result.data.access_token));
    }));
  }
}

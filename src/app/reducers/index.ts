import { ActionReducerMap } from "@ngrx/store";
import { productReducer } from "./product.reducer";
import { cartReducer } from "./cart.reducer";
import { bookingReducer } from "./booking.reducer";


interface AppState {
    product: any;
    cart: any;
    booking:any
  }
export const reducers: ActionReducerMap<AppState> = {
    product: productReducer,
    cart: cartReducer,
    booking:bookingReducer
};

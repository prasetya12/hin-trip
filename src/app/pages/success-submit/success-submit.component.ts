import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-success-submit',
  templateUrl: './success-submit.component.html',
  styleUrls: ['./success-submit.component.scss']
})
export class SuccessSubmitComponent implements OnInit {
  email:any= "halo"
  penginapan:any = ""
  nama_penginapan:any= ""
  no_pemilik:any = ""
  alamat_pemilik:any=""
  longlat:any=""
  kabkot:any=""
  kecamatan:any = ""
  jumlah_kamar_ac:any = ""
  jumlah_kamar_non_ac:any = ""
  harga_ac:any = ""
  harga_non_ac:any = ""
  tempat_tidur:any = ""
  kamar_mandi:any=""
  tipe_toilet:any=""
  fasilitas:any=""
  atraksi:any=""


  constructor(private router: Router) { 
    this.email = this.router.getCurrentNavigation().extras.state.email || '';
    this.penginapan = this.router.getCurrentNavigation().extras.state.penginapan || '';
    this.nama_penginapan = this.router.getCurrentNavigation().extras.state.nama_penginapan || '';
    this.no_pemilik = this.router.getCurrentNavigation().extras.state.no_pemilik || '';
    this.alamat_pemilik = this.router.getCurrentNavigation().extras.state.alamat_pemilik || '';
    this.longlat = this.router.getCurrentNavigation().extras.state.long_lat || '';
    this.kabkot = this.router.getCurrentNavigation().extras.state.kabkot || '';
    this.kecamatan = this.router.getCurrentNavigation().extras.state.kecamatan || '';
    this.jumlah_kamar_ac = this.router.getCurrentNavigation().extras.state.jumlah_ac || '';
    this.jumlah_kamar_non_ac = this.router.getCurrentNavigation().extras.state.jumlah_non_ac || '';
    this.harga_ac = this.router.getCurrentNavigation().extras.state.harga_ac || '';
    this.harga_non_ac = this.router.getCurrentNavigation().extras.state.harga_non_ac || '';
    this.harga_non_ac = this.router.getCurrentNavigation().extras.state.harga_non_ac || '';
    this.tempat_tidur = this.router.getCurrentNavigation().extras.state.tempat_tidur.toString() || '';
    
    this.kamar_mandi = this.router.getCurrentNavigation().extras.state.kamar_mandi || '';
    this.tipe_toilet = this.router.getCurrentNavigation().extras.state.tipe_toilet || '';
    this.fasilitas = this.router.getCurrentNavigation().extras.state.fasilitas || '';
    this.atraksi = this.router.getCurrentNavigation().extras.state.atraksi || '';


    console.log(this.router.getCurrentNavigation().extras.state,'asdads');
  }

  ngOnInit(): void {
  }

  backToHome() {
    this.router.navigateByUrl('/');
    
  }

}

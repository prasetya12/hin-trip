import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrasiHomestayComponent } from './registrasi-homestay.component';

describe('RegistrasiHomestayComponent', () => {
  let component: RegistrasiHomestayComponent;
  let fixture: ComponentFixture<RegistrasiHomestayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrasiHomestayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrasiHomestayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

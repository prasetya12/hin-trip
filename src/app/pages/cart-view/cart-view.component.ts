import { Component, OnInit } from '@angular/core';
import {select,Store} from '@ngrx/store'
import {loadCart, deleteCart,plusCart,minusCart} from '../../actions/cart.actions'
import {createBooking} from '../../actions/booking.actions'

import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-cart-view',
  templateUrl: './cart-view.component.html',
  styleUrls: ['./cart-view.component.scss']
})
export class CartViewComponent implements OnInit {
  cart:any
  formDetail:FormGroup
  submitted=false
  grandtotal:number
  modalReference :any;
  closeResult = '';



  constructor(private store:Store<any>,private formBuilder:FormBuilder,private router: Router,private modalService: NgbModal) { }

  ngOnInit(): void {
    this.store.dispatch(new loadCart())
    this.store.subscribe(state=>(this.cart=state.cart.cart))
    this.grandtotal = this.grandTotal(this.cart)

    this.formDetail = this.formBuilder.group({
      name:['',Validators.required],
      email:['',[Validators.required,Validators.email]],
      phone:['',[Validators.required,Validators.pattern('[0-9]*')]],
      city:['',Validators.required],
    })
  }

  formatNumber(number){
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const num = convert.join('.').split('').reverse().join('')
      return num;
  }

  grandTotal(arr) {
    return arr.reduce((sum, i) => {
      return sum + (i.originalPrice * i.quantity)
    }, 0)
  };

  deleteCart(index){
    this.store.dispatch(new deleteCart(index))
    this.grandtotal = this.grandTotal(this.cart)
  }

  get f() { return this.formDetail.controls; }

  checkout(content){
    this.submitted = true;

    // stop here if form is invalid
    if (this.formDetail.invalid) {
        return;
    }
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  toPayment(){
    let booking=null
    const ticketTypes = this.cart.map((item,i) =>({
        index:i,
        id:item.id,
        quantity:item.quantity,
        fromResellerId:null

    }));

    const data = {  
      "paymentMethod": "CREDIT",
      "customerName":this.f.name.value,
      "email":this.f.email.value,
      "mobileNumber":this.f.phone.value,
      "ticketTypes":ticketTypes
    }

    this.store.dispatch(new createBooking(data))

    this.store.subscribe(state=>
    {
      if(state.booking.dataSuccess){
        this.router.navigate(['/payment/ref/'+state.booking.dataSuccess.reference_number]);

        console.log(state.booking.dataSuccess)
      }
    })

    console.log(booking,'books')
    this.modalReference.close();

  }

  plus(id){
    this.store.dispatch(new plusCart(id))
    this.grandtotal = this.grandTotal(this.cart)
  }

  minus(id){
    this.store.dispatch(new minusCart(id))
    this.grandtotal = this.grandTotal(this.cart)

  }

}

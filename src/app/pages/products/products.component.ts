import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Directive, HostListener, EventEmitter, Output, ElementRef } from '@angular/core';


import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as _ from 'lodash'
import {ProductService} from '../../services/product.service'
import { Observable, of } from 'rxjs';
import { tap, filter, map } from 'rxjs/operators';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Array<any> = [];

  batch = 10
  last= 100
  finished = false
  empty: boolean = false;
  loading: boolean = false;
  closeResult = '';

  constructor(private db:AngularFireDatabase, private productService:ProductService,private modalService: NgbModal) {
    this.getData()
   }

  ngOnInit(): void {
  }

  onScroll(){
    console.log('halo')
    this.getData()
  }

  private getData(key?){
   this.fetchTodosPaginated ()
  }

  scrollHandler(e) { 
    console.log('scroll')
    this.loading = true;
    setTimeout(() => {
      this.fetchTodosPaginated();
      this.loading = false;
    }, 1500);
  }

  fetchTodosPaginated () {
    this.productService.paginate(this.batch, this.last).pipe(
      map(data => {
        if ( !data.length) {
          this.empty = true;
        }
        let last = _.last(data);
        if (last) {
          this.last = last.payload.doc.data().id;
          data.map(todoSnap => {

            this.products.push(todoSnap.payload.doc.data());
            console.log(this.products,'halo')

          })
        }
      })
    ).subscribe();
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  private title(title){
    return title.length>30 ? title.substr(0, 30) + '...':title
  }

  private formatPrice(price){
    var	number_string = price.toString(),
    sisa 	= number_string.length % 3,
    rupiah 	= number_string.substr(0, sisa),
    ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
      
    if (ribuan) {
        var separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    return rupiah
  }

}

import { Component, OnInit,OnDestroy } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Subject } from 'rxjs'
import * as XLSX from "xlsx";
import { FormGroup, FormControl,FormBuilder,FormArray, Validators } from '@angular/forms';


import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-registrasi',
  templateUrl: './list-registrasi.component.html',
  styleUrls: ['./list-registrasi.component.scss']
})
export class ListRegistrasiComponent implements OnInit {
  data :any[] = []
  formlogin : FormGroup;
  isLogin =true;

  constructor(fb: FormBuilder,private firestore: AngularFirestore,private modalService: NgbModal) {

    this.formlogin =fb.group({
      username:['',Validators.required],
      password:['',Validators.required]
    })
   } 
  dtTrigger: Subject<any> = new Subject<any>();
  closeResult = '';


  ngOnInit(): void {
    document.getElementById("openModalButton").click();
    // open(this.content)
    
    this.getRegistrasiHomestay()

   
  }

  getRegistrasiHomestay(){
    this.firestore.collection('registrasi-homestay').get().subscribe((res)=>{
      res.docs.forEach((doc)=>{
        this.data.push(doc.data())
      })
      this.dtTrigger.next();
    })
  }
  
  isFieldValid(field: string) {
    return !this.formlogin.get(field).valid && this.formlogin.get(field).touched;
  }

  loginButton(){
    const username = "reporting@jelajahin.id";
    const password = "juwZ8UEejJQ5J4Tu";
    if(this.formlogin.valid){
      if(this.formlogin.value.username == username && this.formlogin.value.password == password){
        console.log('valid')
        this.modalService.dismissAll()
      }else{
        this.isLogin = false;

      }
      console.log(this.formlogin.value.username)

    }else{
      this.validateAllFormFields(this.formlogin);

    }
  }

  validateAllFormFields(formGroup: FormGroup) { 
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);     
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open(content) {
    this.modalService.open(content, {backdrop: 'static', keyboard: false,ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  
  exportArrayToExcel(){

    let timeSpan = new Date().toISOString();
    let prefix =  "registrasi-homestay";
    let fileName = `${prefix}-${timeSpan}`;

    var wb = XLSX.utils.book_new();
    var ws = XLSX.utils.json_to_sheet(this.data);
    XLSX.utils.book_append_sheet(wb, ws," ExportResult");
    XLSX.writeFile(wb, `${fileName}.xlsx`);
  }

}

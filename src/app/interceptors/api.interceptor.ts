import {Injectable,Injector } from '@angular/core'
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest,HttpClient} from '@angular/common/http'
import {environment} from 'src/environments/environment'
import {finalize,catchError,tap,switchMap} from 'rxjs/operators'
import {Observable} from 'rxjs'
import {API} from '../../constants/urls'
import { AuthService } from "../services/auth.service";


@Injectable()
export class APIInterceptor implements HttpInterceptor{
    isRefreshingToken: boolean = false;

    constructor(private httpClient: HttpClient,private injector: Injector){

    }

    intercept(req:HttpRequest<any>, next : HttpHandler):Observable<HttpEvent<any>>{
        let token = localStorage.getItem('token');

        req = req.clone({
            url: environment.url + `/${req.url}`} );

        if (token) {
            req.clone({
                setHeaders: {
                    'Authorization': 'Bearer ' + token
                }
            } );
        }

        return next.handle(req)
        .pipe(catchError((error, caught) => {
            if(error.status==401){

                return this.handleUnauthorized(req,next);
            }
          return Observable.throw(error);
        })) as any;
        
    }

    private addToken(request: HttpRequest<any>, token: string) {
        return request.clone({
          setHeaders: {
            'Authorization': `Bearer ${token}`
          }
        });
      }

    handleUnauthorized(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        const authService = this.injector.get(AuthService);
            
            return authService.refreshToken().pipe(
                switchMap((res: any) => {
                    if (res) {
                        const newToken = res.data.access_token
                        return next.handle(this.addToken(req, newToken));
                    }

                    // If we don't get a new token, we are in trouble so logout.
                }),
                catchError(error => {
                    // If there is an exception calling 'refreshToken', bad news so logout.
                    throw new Error('Error!');
                }),
                finalize(() => {
                    this.isRefreshingToken = false;
                }),);
    }
}
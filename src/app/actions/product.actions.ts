import {Action} from '@ngrx/store'

export enum AttractionActionTypes {
  GET_ATTRACTIONS = '[PRODUCT] GET_ATTRACTIONS',
  GET_ATTRACTIONS_SUCCESS = '[PRODUCT] GET_ATTRACTIONS_SUCCESS',
  GET_ATTRACTIONS_FAILURE = '[PRODUCT] GET_ATTRACTIONS_FAILURE',

  GET_ACTIVITIES = '[PRODUCT] GET_ACTIVITIES',
  GET_ACTIVITIES_SUCCESS = '[PRODUCT] GET_ACTIVITIES_SUCCESS',
  GET_ACTIVITIES_FAILURE = '[PRODUCT] GET_ACTIVITIES_FAILURE',

  GET_TOURS = '[PRODUCT] GET_TOURS',
  GET_TOURS_SUCCESS = '[PRODUCT] GET_TOURS_SUCCESS',
  GET_TOURS_FAILURE = '[PRODUCT] GET_TOURS_FAILURE',

  GET_DETAIL_ATTRACTION = '[PRODUCT] GET_DETAIL_ATTRACTION',
  GET_DETAIL_ATTRACTION_SUCCESS = '[PRODUCT] GET_DETAIL_ATTRACTION_SUCCESS',
  GET_DETAIL_ATTRACTION_FAILURE = '[PRODUCT] GET_DETAIL_ATTRACTION_FAILURE',

  GET_TICKET_TYPE = '[PRODUCT] GET_TICKET_TYPE',
  GET_TICKET_TYPE_SUCCESS = '[PRODUCT] GET_TICKET_TYPE_SUCCESS',
  GET_TICKET_TYPE_FAILURE = '[PRODUCT] GET_TICKET_TYPE_FAILURE',

  REMOVE_DATA='REMOVE_DATA'

 }

export class getAttractions implements Action{
  readonly type = AttractionActionTypes.GET_ATTRACTIONS
  constructor(public payload:any=null){}

}

export class getAttractionsSuccess implements Action{
  readonly type = AttractionActionTypes.GET_ATTRACTIONS_SUCCESS
  constructor(public payload:any=null){}
}

export class getAttractionsFailure implements Action{
  readonly type = AttractionActionTypes.GET_ATTRACTIONS_FAILURE
  constructor(public payload:any=null){ }
}

export class getActivities implements Action{
  readonly type = AttractionActionTypes.GET_ACTIVITIES
  constructor(public payload:any=null){}

}

export class getActivitiesSuccess implements Action{
  readonly type = AttractionActionTypes.GET_ACTIVITIES_SUCCESS
  constructor(public payload:any=null){}
}

export class getActivitiesFailure implements Action{
  readonly type = AttractionActionTypes.GET_ACTIVITIES_FAILURE
  constructor(public payload:any=null){ }
}

export class getTours implements Action{
  readonly type = AttractionActionTypes.GET_TOURS
  constructor(public payload:any=null){}

}

export class getToursSuccess implements Action{
  readonly type = AttractionActionTypes.GET_TOURS_SUCCESS
  constructor(public payload:any=null){}
}

export class getToursFailure implements Action{
  readonly type = AttractionActionTypes.GET_TOURS_FAILURE
  constructor(public payload:any=null){ }
}

export class getDetailAttraction implements Action{
  readonly type = AttractionActionTypes.GET_DETAIL_ATTRACTION
  constructor(public payload:any=null){ }
}

export class getDetailAttractionSuccess implements Action{
  readonly type = AttractionActionTypes.GET_DETAIL_ATTRACTION_SUCCESS
  constructor(public payload:any=null){}
}

export class getDetailAttractionFailure implements Action{
  readonly type = AttractionActionTypes.GET_DETAIL_ATTRACTION_FAILURE
  constructor(public payload:any=null){}
}

export class getTicketType implements Action{
  readonly type = AttractionActionTypes.GET_TICKET_TYPE
  constructor(public payload:any=null){}
}

export class getTicketTypeSuccess implements Action{
  readonly type = AttractionActionTypes.GET_TICKET_TYPE_SUCCESS
  constructor(public payload:any=null){ }
}

export class getTicketTypeFailure implements Action{
  readonly type = AttractionActionTypes.GET_TICKET_TYPE_FAILURE
  constructor(public payload:any=null){}
}


export class removeData implements Action{
  readonly type = AttractionActionTypes.REMOVE_DATA
}

export type Actions = getAttractions |
                      getAttractionsSuccess | 
                      getAttractionsFailure |
                      getActivities |
                      getActivitiesSuccess | 
                      getActivitiesFailure |
                      getTours |
                      getToursSuccess | 
                      getToursFailure |
                      getDetailAttraction |
                      getDetailAttractionSuccess |
                      getDetailAttractionFailure |
                      getTicketType |
                      getTicketTypeSuccess |
                      getTicketTypeFailure